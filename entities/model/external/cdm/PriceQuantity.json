{
  "classifierPath" : "meta::pure::metamodel::type::Class",
  "content" : {
    "_type" : "class",
    "name" : "PriceQuantity",
    "package" : "model::external::cdm",
    "properties" : [ {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "price",
      "stereotypes" : [ {
        "profile" : "model::external::cdm::metadata",
        "value" : "location"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies a price to be used for trade amounts and other purposes."
      } ],
      "type" : "model::external::cdm::PriceSchedule"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "quantity",
      "stereotypes" : [ {
        "profile" : "model::external::cdm::metadata",
        "value" : "location"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies a quantity to be associated with an event, for example a trade amount."
      } ],
      "type" : "model::external::cdm::NonNegativeQuantitySchedule"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "observable",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the object to be observed for a price, it could be an asset or a reference. The cardinality is optional as some quantity / price cases have no observable (e.g. a notional and a fixed rate in a given currency)."
      } ],
      "type" : "model::external::cdm::Observable"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "buyerSeller",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Defines the direction of the exchange. The convention is that the buyer receives the quantity / pays the price, whereas the seller receives the price / pays the quantity. Attribute is optional in case the price/quantity settlement is defined as part of the product mechanics."
      } ],
      "type" : "model::external::cdm::BuyerSeller"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "settlementTerms",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Whether the settlement is cash or physical and the corresponding terms. Attribute is optional in case the price/quantity settlement is defined as part of the product mechanics."
      } ],
      "type" : "model::external::cdm::SettlementTerms"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "effectiveDate",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the date at which the price and quantity become effective. This day may be subject to adjustment in accordance with a business day convention, or could be specified as relative to a trade date, for instance. Optional cardinality, as the effective date is usually specified in the product definition, so it may only need to be specified as part of the PriceQuantity in an increase/decrease scenario for an existing trade."
      } ],
      "type" : "model::external::cdm::AdjustableOrRelativeDate"
    } ],
    "stereotypes" : [ {
      "profile" : "model::external::cdm::metadata",
      "value" : "key"
    } ],
    "taggedValues" : [ {
      "tag" : {
        "profile" : "meta::pure::profiles::doc",
        "value" : "doc"
      },
      "value" : "Defines a settlement as an exchange between two parties of a specified quantity of an asset (the quantity) against a specified quantity of another asset (the price). The settlement is optional and can be either cash or physical. In the case of non-cash products, the settlement of the price/quantity would not be specified here and instead would be delegated to the product mechanics, as parameterised by the price/quantity values."
    } ]
  }
}