{
  "classifierPath" : "meta::pure::metamodel::type::Class",
  "content" : {
    "_type" : "class",
    "name" : "BasketConstituent",
    "package" : "FpML::model",
    "properties" : [ {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "payerPartyReference",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A reference to the party responsible for making the payments defined by this structure."
      } ],
      "type" : "FpML::model::PartyReference"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "payerAccountReference",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A reference to the account responsible for making the payments defined by this structure."
      } ],
      "type" : "FpML::model::AccountReference"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "receiverPartyReference",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A reference to the party that receives the payments corresponding to this structure."
      } ],
      "type" : "FpML::model::PartyReference"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "receiverAccountReference",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "A reference to the account that receives the payments corresponding to this structure."
      } ],
      "type" : "FpML::model::AccountReference"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "underlyingAsset",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Define the underlying asset, either a listed security or other instrument."
      } ],
      "type" : "FpML::model::Asset"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "constituentWeight",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the weight of each of the underlyer constituent within the basket, either in absolute or relative terms. This is an optional component, as certain swaps do not specify a specific weight for each of their basket constituents."
      } ],
      "type" : "FpML::model::ConstituentWeight"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "dividendPayout",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the dividend payout ratio associated with an equity underlyer. A basket swap can have different payout ratios across the various underlying constituents. In certain cases the actual ratio is not known on trade inception, and only general conditions are then specified. Users should note that FpML makes a distinction between the derivative contract and the underlyer of the contract. It would be better if the agreed dividend payout on a derivative contract was modelled at the level of the derivative contract, an approach which may be adopted in the next major version of FpML."
      } ],
      "type" : "FpML::model::DividendPayout"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "underlyerPrice",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the price that is associated with each of the basket constituents. This component is optional, as it is not absolutely required to accurately describe the economics of the trade, considering the price that characterizes the equity swap is associated to the leg of the trade."
      } ],
      "type" : "FpML::model::Price"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "underlyerNotional",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the notional (i.e. price * quantity) that is associated with each of the basket constituents. This component is optional, as it is not absolutely required to accurately describe the economics of the trade, considering the notional that characterizes the equity swap is associated to the leg of the trade."
      } ],
      "type" : "FpML::model::Money"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "underlyerSpread",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Provides a link to the spread schedule used for this underlyer."
      } ],
      "type" : "FpML::model::SpreadScheduleReference"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "couponPayment",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The next upcoming coupon payment."
      } ],
      "type" : "FpML::model::PendingPayment"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "underlyerFinancing",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Financing terms associated with this underlyer"
      } ],
      "type" : "FpML::model::UnderlyerInterestLeg"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "underlyerLoanRate",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Loan rate terms associated with this underlyer. Commonly used for stock loan. You must not duplicate data elements already contained within dividend conditions at transaction level"
      } ],
      "type" : "FpML::model::UnderlyerLoanRate"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "underlyerCollateral",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Collateral associated with this underlyer. Note that this is not typical usage, collateral is more often at transaction level"
      } ],
      "type" : "FpML::model::Collateral"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "id",
      "type" : "String"
    } ],
    "taggedValues" : [ {
      "tag" : {
        "profile" : "meta::pure::profiles::doc",
        "value" : "doc"
      },
      "value" : "A type describing each of the constituents of a basket."
    } ]
  }
}